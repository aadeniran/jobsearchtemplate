import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';

import { MapPageComponent } from './pages/map-page/map-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import {PageNotFoundComponent} from './pages/page-not-found/page-not-found.component';
import {ApicallsService} from './service/apicalls.service';
import { AppNavbarComponent } from './partial-templates/app-navbar/app-navbar.component';
import {JobSetGetService} from './service/job-set-get.service';

const appRoutes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomePageComponent },
  {
    path: 'map', component: MapPageComponent
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MapPageComponent,
    HomePageComponent,
    PageNotFoundComponent,
    AppNavbarComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [ApicallsService, JobSetGetService],
  bootstrap: [AppComponent]
})
export class AppModule { }
